Feature: Youtube tests

Scenario: Open a video
	# ToDo: implement a step for opening a browser inside the class that inherits from "SpecFlowTestBase"
	# For example: 
	# this.WebDriver = new FirefoxDriver();
	# this.WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
	Given Web browser is opened

	# Generic steps
	And user navigates to 'http://www.youtube.com'
	When user fills 'YoutubePage-SearchInput' with 'Hans Zimmer greatest hits'
	And user clicks 'YoutubePage-OkButton'
	Then the 'YoutubeSearchResultPage-Link' will be displayed
		| LinkText                           |
		| The greatest hits from Hans Zimmer |
	When user clicks 'YoutubeSearchResultPage-Link'
		| LinkText                           |
		| The greatest hits from Hans Zimmer |
	Then the 'YoutubePlayerPage-Player' will be displayed
	Given user closes the web browser
