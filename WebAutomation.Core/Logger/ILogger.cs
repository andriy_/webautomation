﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Logger
{
    /// <summary>
    /// Defines the logger operations.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Set the name of current class.
        /// </summary>
        /// <param name="className">The class name.</param>
        void SetClassName(string className);

        /// <summary>
        /// Write a message with debug level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="parameters">Arguments to format.</param>
        void Debug(string message, params object[] parameters);

        /// <summary>
        /// Write a message with info level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="parameters">Arguments to format.</param>
        void Info(string message, params object[] parameters);

        /// <summary>
        /// Write a message with warn level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="parameters">Arguments to format.</param>
        void Warn(string message, params object[] parameters);

        /// <summary>
        /// Write a message with error level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="parameters">Arguments to format.</param>
        void Error(string message, params object[] parameters);
    }
}
