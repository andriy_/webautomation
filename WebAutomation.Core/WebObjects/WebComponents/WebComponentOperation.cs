﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents
{
    using System;
    using System.Threading;

    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;
    using WebAutomation.Core.WebObjects.WebComponents.WebElementProviders;

    /// <summary>
    /// Represents an operation which can be made on Web Component.
    /// </summary>
    public abstract class WebComponentOperation : IUseWebElementProvider
    {
        /// <summary>
        /// Gets or sets the Web Element provider.
        /// </summary>
        private IWebElementProvider webElementProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentOperation" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="settings">The settings.</param>
        public WebComponentOperation(ILogger logger, ISettings settings)
        {
            this.Logger = logger;
            this.Settings = settings;
            this.Logger.SetClassName(this.GetType().FullName);
        }

        /// <summary>
        /// Gets or sets the Web Element provider.
        /// Should throw an exception when setting null.
        /// </summary>
        public IWebElementProvider WebElementProvider
        {
            get
            {
                return this.webElementProvider;
            }

            set
            {
                AssertHelper.AssertUsage(value != null, "Web Element Provider is not defined", this.Logger);
                this.webElementProvider = value;
            }
        }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        protected ILogger Logger { get; private set; }

        /// <summary>
        /// Gets the settings.
        /// </summary>
        protected ISettings Settings { get; private set; }

        /// <summary>
        /// Gets the Web Element (not null).
        /// </summary>
        protected virtual IWebElement WebElementNotNull
        {
            get
            {
                IWebElement webElement = this.WebElementProvider.WebElement;

                if (webElement == null)
                {
                    string errorMessage = string.Format("Cannot find element\n{0}", this.WebElementProvider.ToString());
                    AssertHelper.AssertTest(false, errorMessage, this.Logger);
                }

                return webElement;
            }
        }

        /// <summary>
        /// Wait for expected result.
        /// </summary>
        /// <param name="func">The function.</param>
        /// <returns>The final result.</returns>
        protected bool WaitForExpectedResult(Func<bool> func)
        {
            int counter = this.Settings.WaitForExpectedResultAttempts;
            while (counter-- > 0)
            {
                bool currentState = func.Invoke();
                if (currentState)
                {
                    return true;
                }

                Thread.Sleep(this.Settings.WaitForExpectedResultsSleepTime);
            }

            return false;
        }
    }
}
