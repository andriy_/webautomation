﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------

namespace WebAutomation.Core.WebObjects.WebComponents.Activities
{
    using WebAutomation.Core.WebObjects.Common.Activities;
    using WebElementProviders;

    /// <summary>
    /// Defines an activity which can be performed during executing an action on WebComponent.
    /// </summary>
    public interface IOnActionActivity : IActivity
    {
        /// <summary>
        /// Gets the type which indicates when the activity must be executed.
        /// </summary>
        ExecutionType ExecutionType { get; }

        /// <summary>
        /// Gets or sets the value of attribute.
        /// </summary>
        string AttributeValue { get; set; }

        /// <summary>
        /// Perform the activity.
        /// </summary>
        /// <param name="webElementProvider">The Web Element Provider.</param>
        void Perform(IWebElementProvider webElementProvider);
    }
}
