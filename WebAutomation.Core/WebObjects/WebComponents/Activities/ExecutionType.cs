﻿namespace WebAutomation.Core.WebObjects.WebComponents.Activities
{
    using System;

    /// <summary>
    /// Indicates when an activity must be executed when performing an action.
    /// </summary>
    [Flags]
    public enum ExecutionType
    {
        /// <summary>
        /// No execution.
        /// </summary>
        None = 0,

        /// <summary>
        /// Execute before the action.
        /// </summary>
        Before = 1,

        /// <summary>
        /// Execute after the action.
        /// </summary>
        After = 2,

        /// <summary>
        /// Execute before and after the action.
        /// </summary>
        BeforeAndAfter = 3
    }
}
