﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Activities
{
    using System;

    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;
    using WebAutomation.Core.WebObjects.WebComponents.Attributes;
    using WebAutomation.Core.WebObjects.WebComponents.WebElementProviders;

    /// <summary>
    /// Set Web Element provider to Web Component.
    /// </summary>
    /// <typeparam name="TRequiredAttribute">Type of required attribute for Web Component.</typeparam>
    public class SetWebElementProviderActivity<TRequiredAttribute> : IWebComponentActivity where TRequiredAttribute : WebComponentAttribute
    {
        /// <summary>
        /// Resolver for Web Element provider.
        /// </summary>
        private Func<IWebElementProvider> webElementProviderResolver;

        /// <summary>
        /// Initializes a new instance of the <see cref="SetWebElementProviderActivity{TRequiredAttribute}" /> class.
        /// </summary>
        /// <param name="webElementProviderResolver">The resolver for Web Element provider.</param>
        /// <param name="logger">The logger.</param>
        public SetWebElementProviderActivity(Func<IWebElementProvider> webElementProviderResolver, ILogger logger)
        {
            this.webElementProviderResolver = webElementProviderResolver;
            this.Logger = logger;
        }

        /// <summary>
        /// Gets the type of the required attribute.
        /// In no attribute is required, null should be returned.
        /// </summary>
        public Type RequiredAttributeType
        {
            get
            {
                return typeof(TRequiredAttribute);
            }
        }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        protected ILogger Logger { get; private set; }

        /// <summary>
        /// Perform the activity on given Web Component.
        /// </summary>
        /// <param name="webComponent">The Web Component.</param>
        /// <param name="webDriver">The Web Driver.</param>
        /// <param name="attributeValue">The value of attribute or null if attribute is no required.</param>
        public void Perform(IWebComponent webComponent, IWebDriver webDriver, string attributeValue = null)
        {
            string webComponentFullName = webComponent.Properties["fullname"];

            // Check if Web Element Provider is already defined for given Web component
            bool isWebElementProviderDefined = webComponent.WebElementProvider != null;
            AssertHelper.AssertUsage(
                !isWebElementProviderDefined,
                string.Format("WebElementProvider is already defined for WebComponent '{0}'. Cannot use multiple providers.", webComponentFullName),
                this.Logger);

            IWebElementProvider webElementProvider = this.webElementProviderResolver.Invoke();
            webElementProvider.Name = webComponentFullName;
            webElementProvider.WebDriver = webDriver;
            webElementProvider.SearchExpression = attributeValue;

            webComponent.WebElementProvider = webElementProvider;
        }
    }
}
