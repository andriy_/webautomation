﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.States
{
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Check (few times) whether the current Web Component will be in a correct state (for example will be displayed on the page).
    /// You can define maximum number of attempts of determining the state in the test settings.
    /// </summary>
    public class WebComponentCheckFutureState : WebComponentCheckCurrentState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentCheckFutureState" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="settings">The settings.</param>
        public WebComponentCheckFutureState(ILogger logger, ISettings settings)
            : base(logger, settings)
        {
        }

        /// <summary>
        /// Check whether the current Web Component will be present (in DOM tree).
        /// To check whether the Web Component will be also displayed (correct CSS), please use Displayed() method instead.
        /// </summary>
        public override bool Present
        {
            get
            {
                return this.WaitForExpectedResult(() => base.Present);
            }
        }

        /// <summary>
        /// Check whether the current Web Component will be not present (in DOM tree).
        /// </summary>
        public override bool NotPresent
        {
            get
            {
                return this.WaitForExpectedResult(() => base.NotPresent);
            }
        }

        /// <summary>
        /// Check whether the current Web Component will be displayed on the page (present in DOM tree + correct CSS).
        /// If the Web Component is displayed, but is currently not visible in the web browser (for example scrolling is needed), the method will still return true.
        /// </summary>
        public override bool Displayed
        {
            get
            {
                return this.WaitForExpectedResult(() => base.Displayed);
            }
        }

        /// <summary>
        /// Check whether the current Web Component will be not displayed on the page (will not be in DOM tree or will have CSS values which makes him hidden).
        /// </summary>
        public override bool NotDisplayed
        {
            get
            {
                return this.WaitForExpectedResult(() => base.NotDisplayed);
            }
        }

        /// <summary>
        /// Check whether the checkbox will be checked.
        /// </summary>
        public override bool Checked
        {
            get
            {
                return this.WaitForExpectedResult(() => base.Checked);
            }
        }

        /// <summary>
        /// Check whether the checkbox will be unchecked.
        /// </summary>
        public override bool NotChecked
        {
            get
            {
                return this.WaitForExpectedResult(() => base.NotChecked);
            }
        }

        /// <summary>
        /// Check whether the Web Component will be enabled.
        /// </summary>
        public override bool Enabled
        {
            get
            {
                return this.WaitForExpectedResult(() => base.Enabled);
            }
        }

        /// <summary>
        /// Check whether the Web Component will be disabled.
        /// </summary>
        public override bool NotEnabled
        {
            get
            {
                return this.WaitForExpectedResult(() => base.NotEnabled);
            }
        }
    }
}
