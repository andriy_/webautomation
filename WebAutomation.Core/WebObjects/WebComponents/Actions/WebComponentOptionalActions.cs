﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Actions
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Activities;
    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Perform an action (for example click, send text) only if the current Web Component exists on the page. 
    /// If Web Component is not present, the action will not be performed and test will be continued without error.
    /// </summary>
    public class WebComponentOptionalActions : WebComponentOperation, IWebComponentActions
    {
        #region Private fields

        /// <summary>
        /// Check action.
        /// </summary>
        private ICheckAction checkAction;

        /// <summary>
        /// Clear action.
        /// </summary>
        private IClearAction clearAction;

        /// <summary>
        /// Click action.
        /// </summary>
        private IClickAction clickAction;

        /// <summary>
        /// Drag and drop action.
        /// </summary>
        private IDragAndDropAction dragAndDropAction;

        /// <summary>
        /// Fill action.
        /// </summary>
        private IFillAction fillAction;

        /// <summary>
        /// Hover action.
        /// </summary>
        private IHoverAction hoverAction;

        /// <summary>
        /// Select action.
        /// </summary>
        private ISelectAction selectAction;

        /// <summary>
        /// Scroll action.
        /// </summary>
        private IScrollAction scrollAction;

        /// <summary>
        /// Gets or sets the list of activities.
        /// </summary>
        private IList<IOnActionActivity> activities;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentOptionalActions" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="settings">The settings.</param>
        /// <param name="checkAction">The check action.</param>
        /// <param name="clearAction">The clear action.</param>
        /// <param name="clickAction">The click action.</param>
        /// <param name="dragAndDropAction">The drag and drop action.</param>
        /// <param name="fillAction">The fill action.</param>
        /// <param name="hoverAction">The hover action.</param>
        /// <param name="selectAction">The select action.</param>
        /// <param name="scrollAction">The scroll action.</param>
        public WebComponentOptionalActions(
            ILogger logger,
            ISettings settings,
            ICheckAction checkAction,
            IClearAction clearAction,
            IClickAction clickAction,
            IDragAndDropAction dragAndDropAction,
            IFillAction fillAction,
            IHoverAction hoverAction,
            ISelectAction selectAction,
            IScrollAction scrollAction)
            : base(logger, settings)
        {
            this.checkAction = checkAction;
            this.clearAction = clearAction;
            this.clickAction = clickAction;
            this.dragAndDropAction = dragAndDropAction;
            this.fillAction = fillAction;
            this.hoverAction = hoverAction;
            this.selectAction = selectAction;
            this.scrollAction = scrollAction;

            this.activities = new List<IOnActionActivity>();
        }

        /// <summary>
        /// Gets the Web Element.
        /// </summary>
        protected virtual IWebElement WebElement
        {
            get
            {
                return this.WebElementProvider.WebElement;
            }
        }

        /// <summary>
        /// Click the web Component.
        /// </summary>
        /// <param name="clickType">Type of click.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public virtual bool Click(ClickType clickType = ClickType.Mouse)
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.clickAction);
            bool success = this.clickAction.Click(clickType);
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", clickType);
            parameters.Add("Result", success);
            this.UpdateLogs("Click the element", parameters);

            return success;
        }

        /// <summary>
        /// Drag and drop current WebComponent to the given WebComponent.
        /// </summary>
        /// <param name="targetComponent">The target component.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public virtual bool DragAndDrop(IWebComponent targetComponent)
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.dragAndDropAction);
            bool success = this.dragAndDropAction.DragAndDrop(targetComponent);
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Target", targetComponent.WebElementProvider.Name);
            parameters.Add("Result", success);
            this.UpdateLogs("Drag and drop", parameters);

            return success;
        }

        /// <summary>
        /// Fill the textbox with given text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="fillType">The way how textbox should be filled with given text.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public virtual bool Fill(string text, FillType fillType = FillType.ReplaceText)
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.fillAction);
            bool success = this.fillAction.Fill(text, fillType);
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Type", fillType);
            parameters.Add("Text", text);
            parameters.Add("Result", success);
            this.UpdateLogs("Fill the element with given text", parameters);

            return success;
        }

        /// <summary>
        /// Clear the textbox.
        /// </summary>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public virtual bool Clear()
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.clearAction);
            bool success = this.clearAction.Clear();
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Result", success);
            this.UpdateLogs("Clear the element", parameters);

            return success;
        }

        /// <summary>
        /// Hover the web element.
        /// </summary>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public virtual bool Hover()
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.hoverAction);
            bool success = this.hoverAction.Hover();
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Result", success);
            this.UpdateLogs("Hover the element", parameters);

            return success;
        }

        /// <summary>
        /// Check the checkbox.
        /// </summary>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public virtual bool Check()
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.checkAction);
            bool success = this.checkAction.Check();
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Result", success);
            this.UpdateLogs("Check the element", parameters);

            return success;
        }

        /// <summary>
        /// Uncheck the checkbox.
        /// </summary>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public virtual bool Uncheck()
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.checkAction);
            bool success = this.checkAction.Uncheck();
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Result", success);
            this.UpdateLogs("Uncheck the element", parameters);

            return success;
        }

        /// <summary>
        /// Select an option by text.
        /// </summary>
        /// <param name="option">The option.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public virtual bool Select(string option)
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.selectAction);
            bool success = this.selectAction.Select(option);
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Option", option);
            parameters.Add("Result", success);
            this.UpdateLogs("Select the option by text", parameters);

            return success;
        }

        /// <summary>
        /// Select an option by index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public virtual bool Select(int index)
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.selectAction);
            bool success = this.selectAction.Select(index);
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Index", index);
            parameters.Add("Result", success);
            this.UpdateLogs("Select the option by index", parameters);

            return success;
        }

        /// <summary>
        /// Scroll to element.
        /// </summary>
        /// <param name="topMargin">Indicates the distance between element and the top edge of the browser after scrolling.</param>
        /// <param name="bottomMargin">Indicates the distance between element and the bottom edge of the browser after scrolling.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public bool Scroll(int topMargin = 100, int bottomMargin = 100)
        {
            this.PerformActivities(ExecutionType.Before);
            this.InitializeAction(this.scrollAction);
            bool success = this.scrollAction.Scroll(topMargin, bottomMargin);
            this.PerformActivities(ExecutionType.After);

            var parameters = new Dictionary<string, object>();
            parameters.Add("Top margin", topMargin);
            parameters.Add("Bottom margin", bottomMargin);
            parameters.Add("Result", success);
            this.UpdateLogs("Scroll to element", parameters);

            return success;
        }

        /// <summary>
        /// Register an activity.
        /// </summary>
        /// <param name="onActionActivity">The OnAction activity.</param>
        public void RegisterActivity(IOnActionActivity onActionActivity)
        {
            this.activities.Add(onActionActivity);
        }

        /// <summary>
        /// Set Web Element.
        /// </summary>
        /// <param name="webElement">The Web Element.</param>
        public void SetWebElement(IWebElement webElement)
        {
        }

        /// <summary>
        /// Set Web Driver.
        /// </summary>
        /// <param name="webDriver">The Web Driver.</param>
        public void SetWebDriver(IWebDriver webDriver)
        {
        }

        /// <summary>
        /// Update logs.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="parameters">The parameters.</param>
        protected void UpdateLogs(string message, Dictionary<string, object> parameters)
        {
            StringBuilder info = new StringBuilder();

            info.AppendFormat("{0}\n", message);
            info.Append(LogFormatter.GetLog(parameters));
            info.Append(this.WebElementProvider.ToString());

            this.Logger.Info(info.ToString());
        }

        /// <summary>
        /// Perform activities.
        /// </summary>
        /// <param name="executionType">The execution type.B</param>
        protected void PerformActivities(ExecutionType executionType)
        {
            var activities = this.activities
                                 .Where(a => (a.ExecutionType & executionType) != ExecutionType.None)
                                 .ToList();

            foreach (var activity in activities)
            {
                activity.Perform(this.WebElementProvider);
            }
        }

        /// <summary>
        /// Initialize the action with correct WebElement and WebDriver.
        /// </summary>
        /// <param name="action">The action.</param>
        protected void InitializeAction(IAction action)
        {
            action.SetWebElement(this.WebElement);
            action.SetWebDriver(this.WebElementProvider.WebDriver);
        }
    }
}
