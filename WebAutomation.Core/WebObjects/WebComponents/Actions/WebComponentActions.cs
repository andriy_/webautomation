﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Actions
{
    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Perform an action (for example click, send text) on the current Web Component.
    /// </summary>
    public class WebComponentActions : WebComponentOptionalActions
    {
       /// <summary>
        /// Initializes a new instance of the <see cref="WebComponentActions" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="settings">The settings.</param>
        /// <param name="checkAction">The check action.</param>
        /// <param name="clearAction">The clear action.</param>
        /// <param name="clickAction">The click action.</param>
        /// <param name="dragAndDropAction">The drag and drop action.</param>
        /// <param name="fillAction">The fill action.</param>
        /// <param name="hoverAction">The hover action.</param>
        /// <param name="selectAction">The select action.</param>
        /// <param name="scrollAction">The scroll action.</param>
        public WebComponentActions(
            ILogger logger,
            ISettings settings,
            ICheckAction checkAction,
            IClearAction clearAction,
            IClickAction clickAction,
            IDragAndDropAction dragAndDropAction,
            IFillAction fillAction,
            IHoverAction hoverAction,
            ISelectAction selectAction,
            IScrollAction scrollAction)
            : base(logger, settings, checkAction, clearAction, clickAction, dragAndDropAction, fillAction, hoverAction, selectAction, scrollAction)
        {
        }

        /// <summary>
        /// Gets the Web Element.
        /// </summary>
        protected override IWebElement WebElement
        {
            get
            {
                return this.WebElementNotNull;
            }
        }
    }
}
