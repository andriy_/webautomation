﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Actions
{
    using OpenQA.Selenium;
    using WebAutomation.Core.WebObjects.Common;

    /// <summary>
    /// Drag and drop the current Web Component to another Web Component.
    /// </summary>
    public class DragAndDropAction : Action, IDragAndDropAction
    {
        /// <summary>
        /// Java Script implementation for simulating Drag And Drop.
        /// <see href="https://gist.github.com/druska/624501b7209a74040175" />
        /// </summary>
        private const string DragAndDropJS = @"     
            function simulateDragDrop(sourceNode, destinationNode) {
                var EVENT_TYPES = {
                    DRAG_END: 'dragend',
                    DRAG_START: 'dragstart',
                    DROP: 'drop'
                }
            
                function createCustomEvent(type) {
                    var event = document.createEvent('CustomEvent');
                    event.initCustomEvent(type, true, true, null);
                    event.dataTransfer = {
                        data: {
                        },
                        setData: function(type, val) {
                            this.data[type] = val
                        },
                        getData: function(type) {
                            return this.data[type]
                        }
                    }
            		
                    return event;
                }
            
                function dispatchEvent(node, type, event) {
                    if (node.dispatchEvent) {
                        return node.dispatchEvent(event)
                    }
            		
                    if (node.fireEvent) {
                        return node.fireEvent('on' + type, event)
                    }
                }
            
                var event = createCustomEvent(EVENT_TYPES.DRAG_START)
                dispatchEvent(sourceNode, EVENT_TYPES.DRAG_START, event)
            
                var dropEvent = createCustomEvent(EVENT_TYPES.DROP)
                dropEvent.dataTransfer = event.dataTransfer
                dispatchEvent(destinationNode, EVENT_TYPES.DROP, dropEvent)
            
                var dragEndEvent = createCustomEvent(EVENT_TYPES.DRAG_END)
                dragEndEvent.dataTransfer = event.dataTransfer
                dispatchEvent(sourceNode, EVENT_TYPES.DRAG_END, dragEndEvent)
            }
            
            simulateDragDrop(arguments[0], arguments[1]);";

        /// <summary>
        /// The settings.
        /// </summary>
        private ISettings settings;

        /// <summary>
        /// Initializes a new instance of the <see cref="DragAndDropAction" /> class.
        /// </summary>      
        /// <param name="settings">The settings.</param>
        public DragAndDropAction(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Drag and drop the current Web Component to the given Web Component.
        /// </summary>
        /// <param name="targetComponent">The target Web Component.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public bool DragAndDrop(IWebComponent targetComponent)
        {
            var targetWebElement = targetComponent.WebElementProvider.WebElement;
            if (this.WebElement == null || targetWebElement == null)
            {
                return false;
            }

            this.WebElement.ScrollToMargin(this.WebDriver, this.settings.TopScrollMargin, this.settings.BottomScrollMargin);
            
            // Selenium implementation (2.49.0) of D&D doesn't work on HTML5
            // Temporary workaround below
            IJavaScriptExecutor js = (IJavaScriptExecutor)this.WebDriver;
            var result = js.ExecuteScript(DragAndDropJS, this.WebElement, targetWebElement);
            return true;
        }
    }
}
