﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Actions
{
    using WebAutomation.Core.WebObjects.Common;

    /// <summary>
    /// Fill the textbox with the given text.
    /// </summary>
    public class FillAction : Action, IFillAction
    {
        /// <summary>
        /// Fill the textbox with the given text.
        /// </summary>
        /// <param name="text">The text used for filling the textbox.</param>
        /// <param name="fillType">Defines the way how a textbox should be filled with a new value.</param>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public bool Fill(string text, FillType fillType = FillType.ReplaceText)
        {
            if (this.WebElement == null)
            {
                return false;
            }

            int textVerifications = 10;
            switch (fillType)
            {
                case FillType.AppendText:
                    if (string.IsNullOrEmpty(text))
                    {
                        return false;
                    }

                    string newValue = this.WebElement.GetValue() + text;
                    while (textVerifications-- > 0)
                    {
                        this.WebElement.Clear();
                        this.WebElement.SendKeys(newValue);

                        if (this.WebElement.GetValue() == newValue)
                        {
                            return true;
                        }
                    }

                    return false;
                case FillType.ReplaceText:
                    while (textVerifications-- > 0)
                    {
                        this.WebElement.Clear();

                        if (!string.IsNullOrEmpty(text))
                        {
                            this.WebElement.SendKeys(text);
                        }

                        if (this.WebElement.GetValue() == text)
                        {
                            return true;
                        }
                    }

                    return false;
                case FillType.SendKeys:
                    if (string.IsNullOrEmpty(text))
                    {
                        return false;
                    }

                    this.WebElement.SendKeys(text);
                    return true;
            }

            return false;
        }
    }
}
