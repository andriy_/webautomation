﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents
{
    using WebAutomation.Core.WebObjects.WebComponents.States;
    using WebAutomation.Core.WebObjects.WebComponents.Value;

    /// <summary>
    /// Assert that the current Web Component is in correct state or has a correct value.
    /// </summary>
    public interface IAssert
    {
        /// <summary>
        /// Assert that the current Web Component is in a correct state (for example is displayed on the page).
        /// </summary>
        IAssertState Is { get; }

        /// <summary>
        /// Assert that the current Web Component will be in a correct state (for example will be displayed on the page).
        /// You can define maximum number of attempts of determining the state in the test settings.
        /// </summary>
        IAssertState WillBe { get; }

        /// <summary>
        /// Assert that the current Web Component has a correct value (for example text, CSS).
        /// </summary>
        IAssertValue Has { get; }

        /// <summary>
        /// Assert that the current Web Component will have a correct value (for example text, CSS).
        /// You can define maximum number of attempts of determining the value in the test settings.
        /// </summary>
        IAssertValue WillHave { get; }
    }
}
