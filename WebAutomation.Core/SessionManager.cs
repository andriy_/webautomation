﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using OpenQA.Selenium;

    /// <summary>
    /// The session manager allows you to use multiple Web Drivers during a test.
    /// </summary>
    public class SessionManager : ISessionManager
    {
        /// <summary>
        /// Gets the name of the default Web Driver session.
        /// </summary>
        public const string DefaultSessionName = "default";

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionManager" /> class.
        /// </summary>
        /// <param name="webDriverChangedAction">The action invoked when changing Web Driver.</param>
        public SessionManager(Action<IWebDriver> webDriverChangedAction)
        {
            this.WebDriverChanged = webDriverChangedAction;
            this.Sessions = new Dictionary<string, IWebDriver>();
            this.SwitchToDefaultSession();
        }

        /// <summary>
        /// Gets the available sessions.
        /// </summary>
        public IReadOnlyList<string> AvailableSessionsNames
        {
            get
            {
                return this.Sessions.Keys.ToList<string>().AsReadOnly();
            }
        }

        /// <summary>
        /// Gets or sets the WebDriver for the current session.
        /// </summary>
        public IWebDriver CurrentSession
        {
            get
            {
                return this.Sessions[this.CurrentSessionName];
            }

            set
            {
                this.Sessions[this.CurrentSessionName] = value;
                this.WebDriverChanged(value);
            }
        }

        /// <summary>
        /// Gets the name of current session.
        /// </summary>
        public string CurrentSessionName { get; protected set; }

        /// <summary>
        /// Gets the Web Driver sessions.
        /// </summary>
        protected Dictionary<string, IWebDriver> Sessions { get; set; }

        /// <summary>
        /// Gets or sets the action invoked when changing Web Driver.
        /// </summary>
        protected Action<IWebDriver> WebDriverChanged { get; set; }

        /// <summary>
        /// Switch current session to the default session.
        /// </summary>
        public void SwitchToDefaultSession()
        {
            this.SwitchSession(DefaultSessionName);
        }

        /// <summary>
        /// Switch current session to the session with the given name.
        /// </summary>
        /// <param name="sessionName">The name of session.</param>
        /// <param name="createIfDoesntExist">Create a new session if session with the given name doesn't exist.</param>
        public void SwitchSession(string sessionName, bool createIfDoesntExist = true)
        {
            this.CurrentSessionName = sessionName;

            bool sessionExists = this.Sessions.ContainsKey(sessionName);
            if (sessionExists)
            {
                this.WebDriverChanged(this.CurrentSession);
                return;
            }

            if (createIfDoesntExist)
            {
                this.Sessions.Add(sessionName, null);
                this.WebDriverChanged(this.CurrentSession);
                return;
            }

            throw new WebAutomationUsageException("Session does not exist: " + sessionName);
        }
    }
}
