﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents
{
    using WebAutomation.Core;
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Assert helper.
    /// </summary>
    public static class AssertHelper
    {
        /// <summary>
        /// Error message format.
        /// </summary>
        private const string ErrorMessageFormat = "[Invalid usage of WebAutomation] {0}";

        /// <summary>
        /// Assert condition is test.
        /// </summary>
        /// <param name="condition">The condition.</param>
        /// <param name="message">The message.</param>
        /// <param name="logger">The logger.</param>
        public static void AssertTest(bool condition, string message, ILogger logger)
        {
            if (!condition)
            {
                logger.Error(message);
                throw new TestAssertionException(message);
            }
        }

        /// <summary>
        /// Assert that condition is usage of WebAutomation.
        /// </summary>
        /// <param name="condition">The condition.</param>
        /// <param name="message">The message.</param>
        /// <param name="logger">The logger.</param>
        public static void AssertUsage(bool condition, string message, ILogger logger)
        {
            if (!condition)
            {
                string errorMessage = string.Format(ErrorMessageFormat, message);
                logger.Error(errorMessage);
                throw new WebAutomationUsageException(errorMessage);
            }
        }
    }
}
