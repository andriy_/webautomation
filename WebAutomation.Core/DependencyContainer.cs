﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core
{
    using System;

    using Microsoft.Practices.Unity;
    using WebAutomation.Core.Logger;
    using WebAutomation.Core.WebObjects.Common.Activities;
    using WebAutomation.Core.WebObjects.Manager;
    using WebAutomation.Core.WebObjects.WebComponents;
    using WebAutomation.Core.WebObjects.WebComponents.Actions;
    using WebAutomation.Core.WebObjects.WebComponents.Activities;
    using WebAutomation.Core.WebObjects.WebComponents.Attributes;
    using WebAutomation.Core.WebObjects.WebComponents.States;
    using WebAutomation.Core.WebObjects.WebComponents.Value;
    using WebAutomation.Core.WebObjects.WebComponents.WebElementProviders;
    using WebAutomation.Core.WebObjects.WebContainer.Activities;

    /// <summary>
    /// Dependency injection.
    /// </summary>
    internal static class DependencyContainer
    {
        /// <summary>
        /// Initialize container.
        /// </summary>
        /// <returns>Unity container.</returns>
        public static IUnityContainer InitContainer()
        {
            var container = new UnityContainer();
 
            container.RegisterInstance<ISettings>(new Settings(4000, 500, 100, 100, 6, 500));
           
            container.RegisterType<ILogger, ConsoleLogger>();
            container.RegisterType<ISessionManager, SessionManager>();
            container.RegisterType<IExtensionsManager, ExtensionsManager>();
            container.RegisterType<IActivityResolver, ActivityResolver>();
            container.RegisterType<IWebObjectsManager, WebObjectsManager>();
            container.RegisterType<IWebComponent, WebComponent>();

            container.RegisterType<IWebComponentAssert, WebComponentAssert>();
            container.RegisterType<IWebComponentAssertState, WebComponentAssertState>();
            container.RegisterType<IWebComponentAssertValue, WebComponentAssertValue>();

            container.RegisterType<IWebComponentActions, WebComponentActions>("actionPerform");
            container.RegisterType<IWebComponentActions, WebComponentOptionalActions>("actionPerformIfExists");
            container.RegisterType<IWebComponentCheckState, WebComponentCheckCurrentState>("stateIs");
            container.RegisterType<IWebComponentCheckState, WebComponentCheckFutureState>("stateWillBe");
            container.RegisterType<IWebComponentCheckValue, WebComponentCheckCurrentValue>("valueHas");
            container.RegisterType<IWebComponentCheckValue, WebComponentCheckFutureValue>("valueWillHave");

            container.RegisterType<ICheckAction, CheckAction>();
            container.RegisterType<IClearAction, ClearAction>();
            container.RegisterType<IClickAction, ClickAction>();
            container.RegisterType<IDragAndDropAction, DragAndDropAction>();
            container.RegisterType<IFillAction, FillAction>();
            container.RegisterType<IHoverAction, HoverAction>();
            container.RegisterType<ISelectAction, SelectAction>();
            container.RegisterType<IScrollAction, ScrollAction>();

            container.RegisterType<IActivity, CreateWebComponentsActivity>("CreateWebComponents");

            container.RegisterType<IWebElementProvider, WebElementByIdProvider>("byId");
            container.RegisterType<IWebElementProvider, WebElementByPidProvider>("byPid");
            container.RegisterType<IWebElementProvider, WebElementByClassProvider>("byClass");
            container.RegisterType<IWebElementProvider, WebElementByPclassProvider>("byPclass");
            container.RegisterType<IWebElementProvider, WebElementByCssProvider>("byCss");
            container.RegisterType<IWebElementProvider, WebElementByPCssProvider>("byPcss");
            container.RegisterType<IWebElementProvider, WebElementByXPathProvider>("byXpath");
            container.RegisterType<IWebElementProvider, WebElementByPXPathProvider>("byXxpath");
            container.RegisterType<IWebElementProviderWithIframe, WebElementProviderWithIframe>();

            container.RegisterType<IActivity, SetWebElementProviderActivity<IdAttribute>>(
                "id",
                new InjectionConstructor(
                    new ResolvedParameter<Func<IWebElementProvider>>("byId"),
                    new ResolvedParameter<ILogger>()));

            container.RegisterType<IActivity, SetWebElementProviderActivity<PidAttribute>>(
                "pid",
                new InjectionConstructor(
                    new ResolvedParameter<Func<IWebElementProvider>>("byPid"),
                    new ResolvedParameter<ILogger>()));

            container.RegisterType<IActivity, SetWebElementProviderActivity<ClassAttribute>>(
                "class",
                new InjectionConstructor(
                    new ResolvedParameter<Func<IWebElementProvider>>("byClass"),
                    new ResolvedParameter<ILogger>()));

            container.RegisterType<IActivity, SetWebElementProviderActivity<PclassAttribute>>(
                "pclass",
                new InjectionConstructor(
                    new ResolvedParameter<Func<IWebElementProvider>>("byPclass"),
                    new ResolvedParameter<ILogger>()));

            container.RegisterType<IActivity, SetWebElementProviderActivity<CssAttribute>>(
                "css",
                new InjectionConstructor(
                    new ResolvedParameter<Func<IWebElementProvider>>("byCss"),
                    new ResolvedParameter<ILogger>()));

            container.RegisterType<IActivity, SetWebElementProviderActivity<PcssAttribute>>(
                "pcss",
                new InjectionConstructor(
                    new ResolvedParameter<Func<IWebElementProvider>>("byPcss"),
                    new ResolvedParameter<ILogger>()));

            container.RegisterType<IActivity, SetWebElementProviderActivity<XpathAttribute>>(
                "xpath",
                new InjectionConstructor(
                    new ResolvedParameter<Func<IWebElementProvider>>("byXpath"),
                    new ResolvedParameter<ILogger>()));

            container.RegisterType<IActivity, SetWebElementProviderActivity<PxpathAttribute>>(
                "pxpath",
                new InjectionConstructor(
                    new ResolvedParameter<Func<IWebElementProvider>>("byXxpath"),
                    new ResolvedParameter<ILogger>()));

            container.RegisterType<IActivity, UpdateProvidersForIframesActivity>("iframe");
            container.RegisterType<IActivity, AssertNotPresentActivity>("AssertNotPresent");
            container.RegisterType<IActivity, AssertPresentActivity>("AssertPresent");

            return container;
        }
    }
}
